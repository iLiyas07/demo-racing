import './index.less';
import World from "./js/World";
import Box2d from "./libs/Box2d";
import {error} from "autoprefixer/lib/utils";

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const drawScale = 10;
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const world = new World(Box2d, drawScale);
world.initDebugDraw(ctx, drawScale);

const car = new Image();
const progressCar = new Image();
const group = new Image();
const bg = new Image();
const info = new Image();
const firstMap = new Image();

car.src = "./images/car.png";
progressCar.src = "./images/car.png";
group.src = "./images/group.png";
bg.src = "./images/Frame1.png";
info.src = "./images/info.png";
firstMap.src = "./images/firstMap.png";

const steps = ["instruction", "game", "end"];
let gameOver = false;
let currentStep = 0;

const maps = [firstMap, firstMap, firstMap];

let upPressed = false;
let downPressed = false;
let rightPressed = false;

let progress = 0;
let mapsWidth = canvas.width * maps.length;

const cameraPos = {
    x: 0,
    y: 0,
};

const textures = [
    { image: group, x: canvas.width + 40, y: canvas.height / 2 - 20, angle: 0 },
    { image: group, x: canvas.width, y: canvas.height / 2 + 20, angle: 0 },
    {
        image: car,
        x: 100,
        y: canvas.height / 2,
        angle: 0,
        isMainCar: true,
    },
];


let time = 0;
!function update() {
    requestAnimationFrame(update);

    const currentTime = performance.now();
    const dt = currentTime - time;
    time = currentTime;
    ctx.save();
    const camX = -world.car.GetPosition().x * drawScale + window.innerWidth / 2;
    ctx.translate(camX, 0);
    ctx.clearRect(-camX, 0, window.innerWidth, window.innerHeight)
    world.update(dt);
    ctx.restore();
    textures[2].x = world.car.GetPosition().x * drawScale - 47;
    textures[2].y = world.car.GetPosition().y * drawScale - 30;
    textures[2].angle = world.car.GetAngle() - Math.PI / 2;
    drawTexture(textures[2], camX);
}();

function rotateImage(image, x, y, angle) {
    ctx.save();
    ctx.translate(x + image.width / 2, y + image.height / 2);
    ctx.rotate(angle);
    ctx.drawImage(image, -image.width / 2, -image.height / 2);
    ctx.restore();
}

function drawTexture(texture, cameraPosX) {
    const { image, x, y, angle, isMainCar } = texture;

    rotateImage(image, x + cameraPosX, y, angle);
}