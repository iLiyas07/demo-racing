const HALF_PI = Math.PI / 2;
export default class World {
    constructor(Engine, scale) {
        this.scale = scale;
        this.World = Engine.Dynamics.b2World;
        this.Vec2 = Engine.Common.Math.b2Vec2;
        this.DebugDraw = Engine.Dynamics.b2DebugDraw;
        this.BodyDef = Engine.Dynamics.b2BodyDef
        this.Body = Engine.Dynamics.b2Body
        this.PolygonShape = Engine.Collision.Shapes.b2PolygonShape;
        this.FixtureDef = Engine.Dynamics.b2FixtureDef;
        this.Listener = Engine.Dynamics.b2ContactListener;

        this.fixDef = new this.FixtureDef();
        this.bodyDef = new this.BodyDef();
        this.debugDraw = new this.DebugDraw();

        this.gasForce = 0.24;

        this.model = new this.World(new this.Vec2(0, 0), false);
        this.car = null;

        this.controls = {
            Arrowap: {
                status: true,
                action: this.gas.bind(this),
            },
            ' ': {
                status: false,
                action: this.brake.bind(this),
            },
            ArrowLeft: {
                status: false,
                action: this.turnLeft.bind(this),
            },
            ArrowRight: {
                status: false,
                action: this.turnRight.bind(this),
            },
        }
        this.wheelAngle = 0;

        this.initCar();
        this.initRoad();
        this.addBox();

        this.cameraX = 0;
    }

    initDebugDraw(ctx, drawScale) {
        this.debugDraw.SetSprite(ctx);
        this.debugDraw.SetDrawScale(drawScale);
        this.debugDraw.SetFillAlpha(0.3);
        this.debugDraw.SetLineThickness(1.0);
        this.debugDraw.SetFlags(this.DebugDraw.e_shapeBit | this.DebugDraw.e_jointBit);
        this.model.SetDebugDraw(this.debugDraw);
    }

    initCar() {
        const size = new this.Vec2(22 / this.scale, 47 / this.scale);

        this.fixDef.density = 100.0;
        this.fixDef.restitution = .001;
        this.fixDef.friction = 0;
        this.fixDef.shape = new this.PolygonShape;
        this.fixDef.shape.SetAsBox(size.x, size.y);

        this.bodyDef.type = this.Body.b2_dynamicBody;
        this.bodyDef.position.Set(window.innerWidth / this.scale - size.x - 1, window.innerHeight / 2 / this.scale - size.y * 7)
        this.bodyDef.angle = -HALF_PI;


        const body = this.model.CreateBody(this.bodyDef);
        const fixture = body.CreateFixture(this.fixDef);
        this.car = body;
        this.initControls();
        this.bodyDef.angle = 0;
    }

    initControls() {
        window.addEventListener('keydown', (e)=> {
            this._setControlStatus(e, true);
        })
        window.addEventListener('keyup', (e)=> {
            this._setControlStatus(e, false);
        })
    }

    _setControlStatus(e, status) {
        const button = e.key;
        if (this.controls[button]) this.controls[button].status = status;
    }

    chekActions() {
        for (let controlsKey in this.controls) {
            const control = this.controls[controlsKey];
            if (control.status) control.action();
        }
        if (!this.controls.ArrowRight.status && !this.controls.ArrowLeft.status) this.wheelAngle *= .7;
    }
    gas()   {
        const direction = this.car.GetAngle() - HALF_PI;
        const pos = this.car.GetPosition();

        this.car.ApplyForce(new this.Vec2( this.gasForce * Math.cos(direction), this.gasForce * Math.sin(direction)), pos);
        this.gasForce = 0.40;
    }

    brake() {
        this.gasForce = .08;
    }

    turnLeft() {
        this.wheelAngle = Math.max(this.wheelAngle - .03, -Math.PI / 9);

    }
    turnRight() {
        this.wheelAngle = Math.min(this.wheelAngle + .03, Math.PI / 9);
    }
    applyTurn() {
        const carV = this.car.GetLinearVelocity();
        const wheelGlobalAng = this.car.GetAngle() + this.wheelAngle - HALF_PI;
        const modCarV = carV.Length();
        const wheelV = new this.Vec2(modCarV * Math.cos(wheelGlobalAng), modCarV * Math.sin(wheelGlobalAng));

        const dV = new this.Vec2(wheelV.x - carV.x,wheelV.y -  carV.y);
        const qef = 15;
        const pos = this.car.GetPosition();
        const dPos = new this.Vec2(3 * Math.cos(this.car.GetAngle() - HALF_PI), 3 * Math.sin(this.car.GetAngle() - HALF_PI));
        this.car.ApplyForce(new this.Vec2(dV.x * qef, dV.y * qef), new this.Vec2(pos.x + dPos.x, pos.y + dPos.y));

        const wheelV2 = new this.Vec2(modCarV * Math.cos(this.car.GetAngle() - HALF_PI), modCarV * Math.sin(this.car.GetAngle() - HALF_PI));
        const dV2 = new this.Vec2(wheelV2.x - carV.x,wheelV2.y -  carV.y);
        this.car.ApplyForce(new this.Vec2(dV2.x * qef * 10, dV2.y * qef * 10), new this.Vec2(pos.x - dPos.x, pos.y - dPos.y));
    }
    applyFriction() {
        const v = this.car.GetLinearVelocity();
        const q = .973;
        this.car.SetLinearVelocity(new this.Vec2(v.x * q, v.y * q));
    }
    update(dt) {
        this.chekActions();
        this.applyTurn(dt)
        this.model.Step(dt, 20, 20);
        this.applyFriction();
        this.model.DrawDebugData();
        this.model.ClearForces();
    }

    initRoad() {
        const size = new this.Vec2(50, 1);

        this.fixDef.shape = new this.PolygonShape;
        this.fixDef.shape.SetAsBox(size.x, size.y);

        this.bodyDef.type = this.Body.b2_staticBody;
        this.bodyDef.position.Set(50, 7)


        const body1 = this.model.CreateBody(this.bodyDef);
        const fixture1 = body1.CreateFixture(this.fixDef);

        this.fixDef.shape = new this.PolygonShape;
        this.fixDef.shape.SetAsBox(size.x, size.y);

        this.bodyDef.type = this.Body.b2_staticBody;
        this.bodyDef.position.Set(50, 27)


        const body2 = this.model.CreateBody(this.bodyDef);
        const fixture2 = body2.CreateFixture(this.fixDef);
    }

    addBox() {
        const size = new this.Vec2(2, 2);

        this.fixDef.density = 300.0;
        this.fixDef.restitution = 1;
        this.fixDef.friction = 2;
        this.fixDef.shape = new this.PolygonShape;
        this.fixDef.shape.SetAsBox(size.x, size.y);

        this.bodyDef.type = this.Body.b2_dynamicBody;

        this.bodyDef.position.Set(15, 14)
        const body1 = this.model.CreateBody(this.bodyDef);
        const fixture1 = body1.CreateFixture(this.fixDef);

        this.bodyDef.position.Set(45, 21)
        const body2 = this.model.CreateBody(this.bodyDef);
        const fixture2 = body2.CreateFixture(this.fixDef);

        this.bodyDef.position.Set(75, 14)
        const body3 = this.model.CreateBody(this.bodyDef);
        const fixture3 = body3.CreateFixture(this.fixDef);
    }
}