# Базовый пакет для разработки баннеров AdFox

## Установка и запуск

Необходимо в пустой папке для проекта выполнить команды:
```bash
$ git archive --remote=git@bitbucket.org:div-production/base-banner-adfox.git master | tar -x
$ npm install
$ npm run dev
```

Чтобы запустить проект на необходимом ip адресе нужно выполнить команду:
```bash
$ npm run dev -- --host=ip адрес
```

## Сборка

Для получения итоговой сборки необходимо выполнить команду:
```bash
$ npm run build
```

После этого в папке dist будут лежать собранные файлы, а также готовый архив, который можно отправлять клиенту

## Ссылки

Ссылки в баннере необходимо размещать следующим образом:
1. Если в баннере одна ссылка - ```<a href="%banner.reference_user1%" target="%banner.target%"></a>```
2. Если в баннере несколько ссылок - ```<a href="%request.reference%@%banner.event1%" target="%banner.target%"></a>```, где 1 это номер события от 1 до 28

Для подсчета событий, их нужно прописать следующим образом:
 ```new Image().src = '%banner.event1%'```, где 1 это номер события от 1 до 28
 
## Костыль для баннера 300x600 на Яндекс.Музыке

```js
if(window.frameElement) {
    window.parent.document.body.style.overflow = 'auto';
    window.frameElement.style.minHeight = '600px';
}
if (window.parent) {
    const styleElement = window.parent.document.body.querySelector('style');

    if (styleElement) {
        const css = styleElement.innerHTML;
        let resultCss = css.replace('@media all and (max-width: 1264px)', '@media all and (max-width: 1262px)');

        resultCss += `body{-ms-overflow-style:none;scrollbar-width:none}body::-webkit-scrollbar{display:none}}`;
        styleElement.innerHTML = resultCss;
    }
}
```